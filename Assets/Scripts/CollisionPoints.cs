﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;


// GIVES POINTS FOR A COLLISION
public class CollisionPoints : MonoBehaviour 
{
	[SerializeField] private float _points;
	[SerializeField] private string _playerTag;
	[SerializeField] private UnityEvent _onCollision;

	private void OnTriggerEnter2D(Collider2D col )
	{
		TryGivePoints ( col );
	}

	private void OnCollisionEnter2D( Collision2D col )
	{
		TryGivePoints (col.collider);
	}

	private void TryGivePoints( Collider2D col )
	{
		if (string.IsNullOrEmpty (_playerTag) || col.gameObject.tag == _playerTag) {
			
			var data = FindObjectOfType<PlayerData> ();
			if (data != null) {
				Debug.Log ("points awarded " + _points);
				data.Points += _points;

				_onCollision.Invoke ();
			}
		}
	}

}
