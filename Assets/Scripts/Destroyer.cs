﻿using UnityEngine;
using System.Collections;

public class Destroyer : MonoBehaviour 
{
	[SerializeField] private GameObject _target;
	private bool _finished;

	void Awake () 
	{
		if( _target == null )
		{
			_target = gameObject;
		}
	}

	public void Run()
	{
		if( !_finished )
		{
			Destroy (_target);
			_finished = true;
		}
	}
}
