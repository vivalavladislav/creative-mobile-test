﻿using UnityEngine;
using System.Collections;

public class Spring : MonoBehaviour 
{
	[SerializeField] private string _tag;
	[SerializeField] private float _minForce;
	[SerializeField] private float _maxForce;
	[SerializeField] private float _targetTime;
	[SerializeField] private Color _targetColor;
	[SerializeField] private UnityEngine.Events.UnityEvent _onFire;
	[SerializeField] public UnityEngine.Events.UnityEvent OnBallOnTop;

	private float _time;
	private bool _ballOnTop;
	private bool _trackMouse;
	private bool _activated;

	private SpriteRenderer _sprite;
	private Color _fromColor;


	public float TargetTime
	{
		get{ return _targetTime; }
	}


	// receives from input mediator 
	public void TrackMouse(bool track)
	{
		_trackMouse = track;
	}

	public void Charge( bool value )
	{
		if ( !_activated && value )
		{
			_time = 0;
		}
		if ( _activated && !value)
		{
			Fire ();
		}

		_activated = value;
	}

	private void Awake()
	{
		_sprite = GetComponent<SpriteRenderer> ();
		if (_sprite != null )
		{
			_fromColor = _sprite.color;
		}
	}


	// ENABLE CHARGING WHEN BALL ON TOP
	private void OnCollisionEnter2D( Collision2D col )
	{
		if ( col != null && col.gameObject.tag == _tag )
		{
			_ballOnTop = true;
			OnBallOnTop.Invoke ();
		}
	}

	private void OnCollisionExit2D( Collision2D col )
	{
		if ( col != null && col.gameObject.tag == _tag )
		{
			_ballOnTop = false;
		}
	}

	void Update () 
	{
		if ( !_ballOnTop )
		{
			return;
		}
			
		if ( _trackMouse )
		{
			TrackMouseClick ();
		}


		// CHARGING TIMER AND COLORS
		if( _activated )
		{
			_time += Time.deltaTime;

			// update sprite color
			if (_sprite != null)
			{
				var coef = Mathf.Clamp01 (_time / _targetTime);
				_sprite.color = Color.Lerp( _fromColor, _targetColor, coef);
			}
		}

	}

	// TRACKS IF COLLIDER WAS CLICKED/TAPPED
	private void TrackMouseClick()
	{
		bool activated = false;

		if ( Input.GetMouseButton(0) )
		{
			var wp = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			var touchPos = new Vector2(wp.x, wp.y);

			if (GetComponent<Collider2D>() == Physics2D.OverlapPoint(touchPos))
			{
				activated = true;
			}
		}

		Charge (activated);
	}


	private void Fire()
	{
		if (!_ballOnTop)
			return;
		
		var obj = GameObject.FindGameObjectWithTag (_tag);
		if ( obj == null ){
			Debug.LogError ("No " + _tag + " object. Can't apply spring.");
			return;
		}

		var body = obj.GetComponent<Rigidbody2D> ();
		if ( body == null )
		{
			Debug.LogError (obj.name + " doesn't have rigidbody2d component. can't apply spring");
			return;
		}

		var coef = Mathf.Clamp (_time, 0, _targetTime);
		var force = Mathf.Lerp (_minForce, _maxForce, coef);

		body.AddForce (Vector2.up * force, ForceMode2D.Force);

		// set default sprite color
		if (_sprite !=null )
		{
			_sprite.color = _fromColor;
		}

		_onFire.Invoke ();
	}

}
