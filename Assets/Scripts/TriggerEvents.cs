﻿using UnityEngine;
using System.Collections;

public class TriggerEvents : MonoBehaviour 
{
	[SerializeField] private string _tag;
	[SerializeField] private UnityEngine.Events.UnityEvent _onEnter;

	public event System.Action< GameObject, Collider2D> OnEnter = delegate{};
		
	private void OnTriggerEnter2D( Collider2D col )
	{
		if ( string.IsNullOrEmpty( _tag ) || col.gameObject.tag == _tag )
		{
			_onEnter.Invoke ();
			OnEnter (gameObject, col);
		}
	}

}
