﻿using UnityEngine;
using System.Collections;

public class AISpring : MonoBehaviour 
{
	[SerializeField] private Spring _spring;

	private float _timer;
	private float _timeToWait = -1;

	private void Awake()
	{
		if ( _spring == null )
		{
			_spring = FindObjectOfType<Spring> ();
		}

		if ( _spring != null )
		{
			_spring.OnBallOnTop.AddListener (BallOnTop);
		}

	}

	private void BallOnTop()
	{
		if (_spring == null)
			return;
		
		var random = new System.Random ();
		_timeToWait = (float)random.NextDouble () * _spring.TargetTime;
		_timer = 0;

		_spring.Charge (true);
	}

	private void Update()
	{
		if (_timeToWait < 0)
			return;
		
		_timer += Time.deltaTime;
		if ( _timer >= _timeToWait )
		{
			_timer = 0;
			_timeToWait = -1;
			_spring.Charge (false);
		}
	}

}
