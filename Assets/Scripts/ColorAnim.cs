﻿using UnityEngine;
using System.Collections;

// CHANGES THE COLOR OF A SPRITE TO TARGET WITH GIVEN TIME 
// AND THEN CHANGES BACK TO ORIGINAL COLOR IF CHANGEBACK IS SELECTED
public class ColorAnim : MonoBehaviour 
{
	[SerializeField] private SpriteRenderer _sprite;
	[SerializeField] private Color _to;
	[SerializeField] private float _totalTime;
	[SerializeField] private bool _changeBack = true;

	private Color _from;
	private bool _running = false ;
	private float _time;
	private bool _reversed;

	private void Start()
	{
		if ( _sprite == null )
		{
			_sprite = GetComponent<SpriteRenderer> ();
		}

		_from = _sprite.color;
		Run ();
	}


	public void Run()
	{
		if (_running)
			return;
		
		_running = true;
		_time = 0;
		_reversed = false;
	}

	private void Update () 
	{
		if ( _sprite == null || !_running )
		{
			return;
		}
			
		var dt = _reversed ? -Time.deltaTime : Time.deltaTime;

		_time += dt;
		_time = Mathf.Clamp (_time, 0, _totalTime);
		_sprite.color = Color.Lerp (_from, _to, _time/ _totalTime);

		if ( _time <= 0 && _reversed )
		{
			_running = false;
		}
			
		if ( _time >= _totalTime )
		{
			_reversed = _changeBack;
			_running = _changeBack;
		}
	}
}
