﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour 
{
	[SerializeField] private PlayerData _player;
	[SerializeField] private Text _balls;
	[SerializeField] private Text _score;

	void Awake () 
	{
		if ( _player == null  )
		{
			_player = FindObjectOfType< PlayerData> ();
		}

		if ( _player != null )
		{ 
			_player.OnPointsChanged.AddListener (OnScore);
			_player.OnBallsNumberChanged.AddListener (OnBallLost);

			OnBallLost ();
		}
	}

	void OnScore()
	{
		_score.text = _player.Points.ToString ();
	}

	void OnBallLost()
	{
		_balls.text = _player.RemainingBalls.ToString ();
	}

}
