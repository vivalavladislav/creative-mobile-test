﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour 
{
	[SerializeField] private string _tag;
	[SerializeField] private GameObject _prefab;
	[SerializeField] private Transform _targetPoint;

	public event System.Action<GameObject> Spawned = delegate {};

	public void Spawn()
	{
		var obj = GameObject.FindGameObjectWithTag (_tag);
		if ( obj != null )
		{
			Destroy (obj.gameObject);
		}

		var spawned = Instantiate (_prefab, _targetPoint.position, _targetPoint.rotation) as GameObject;

		Spawned( spawned );
	}
}
