﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

// STORES NUMBERS
public class PlayerData : MonoBehaviour 
{
	[SerializeField] private int _defaultBallsNumber;
	[SerializeField] private int _currentBallsNumber;
	[SerializeField] private float _pointsEarned;
	[SerializeField] private UnityEvent _onSpawnBall;
	[SerializeField] private UnityEvent _onGameEnd;
	public UnityEvent OnPointsChanged;
	public UnityEvent OnBallsNumberChanged;

	public float Points
	{
		get{ return _pointsEarned; }
		set{
			_pointsEarned = value;
			OnPointsChanged.Invoke ();
		}
	}

	public int RemainingBalls
	{
		get{ return _currentBallsNumber; }
	}


	private void Start()
	{
		_currentBallsNumber = _defaultBallsNumber - 1;
		_onSpawnBall.Invoke ();
		OnBallsNumberChanged.Invoke ();
	}


	public void BallLost ()
	{
		if ( _currentBallsNumber <= 0 )
		{
			_onGameEnd.Invoke ();
		}
		else
		{
			--_currentBallsNumber;
			_onSpawnBall.Invoke ();
			OnBallsNumberChanged.Invoke ();
		}
	}

}
