﻿using UnityEngine;
using System.Collections;

public class AI : MonoBehaviour 
{
	[SerializeField] private FLIPPER _flipper;

	[SerializeField] private TriggerEvents _tip;
	[SerializeField] private TriggerEvents _center;
	[SerializeField] private TriggerEvents _back;

	[Header("from 0 to 1")]
	[SerializeField] private float _usageProbabilityTip;
	[SerializeField] private float _usageProbabilityCenter;
	[SerializeField] private float _usageProbabilityBack;

	private System.Random _random = new System.Random();

	private bool _shooting;

	void Start () 
	{
		_tip.OnEnter += (g,b ) => TryFire(_usageProbabilityTip);
		_center.OnEnter += (a, b) => TryFire (_usageProbabilityCenter);
		_back.OnEnter += (a,b) => TryFire( _usageProbabilityBack );
	}

	private void TryFire( float probability )
	{
		if ( _shooting )
			return;
		
		var fire = _random.NextDouble () <= probability;

		if ( fire )
		{
			_shooting = true;
			_flipper.Activate (true);	
		}
	}

	// CHANGES WHEN ACTUAL RIGIDBODY PROPERTIES CHANGE
	private bool _shootingStarted;
	private void LateUpdate()
	{
		var moving = _flipper.GetComponent<Rigidbody2D> ().velocity.magnitude >= Mathf.Epsilon;

		if( !_shootingStarted )
		{
			if (  moving )
			{
				_shootingStarted = true;
			}
		}

		if ( !moving && _shootingStarted )
		{
			_shootingStarted = false;
			_shooting = false;
			_flipper.Activate (false);
		}
	}


}
