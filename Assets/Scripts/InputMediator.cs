﻿using UnityEngine;
using System.Collections.Generic;

// EXPOSES KEY UP/DOWN FUNCTIONS TO EDITOR
public class InputMediator : MonoBehaviour 
{
	[System.Serializable]
	public struct InputPair
	{
		public KeyCode Key;
		public UnityEngine.Events.UnityEvent OnDown;
		public UnityEngine.Events.UnityEvent OnUp;
	}

	[System.Serializable]
	public struct BtnsPair
	{
		public int ButtonIndex;
		public UnityEngine.Events.UnityEvent OnDown;
		public UnityEngine.Events.UnityEvent OnUp;
	}

	[SerializeField] private List<InputPair> _keys;
	[SerializeField] private List<BtnsPair> _btns;

	void Update () 
	{
		// keys
		foreach( var pair in _keys )	
		{
			if ( Input.GetKeyDown(pair.Key))
			{
				pair.OnDown.Invoke ();
			}

			if ( Input.GetKeyUp(pair.Key))
			{
				pair.OnUp.Invoke ();
			}

		}

		// keys
		foreach( var pair in _btns )	
		{
			if ( Input.GetMouseButtonDown(pair.ButtonIndex))
			{
				pair.OnDown.Invoke ();
			}

			if ( Input.GetMouseButtonUp(pair.ButtonIndex))
			{
				pair.OnUp.Invoke ();
			}

		}

	}
}
