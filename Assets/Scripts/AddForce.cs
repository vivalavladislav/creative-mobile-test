﻿using UnityEngine;
using System.Collections;

public class AddForce : MonoBehaviour 
{
	[SerializeField] private Vector2 _force;
	[SerializeField] private ForceMode2D _mode;


	void Start () 
	{
		GetComponent<Rigidbody2D> ().AddForce (_force, _mode);
	}
}
