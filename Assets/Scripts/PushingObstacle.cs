﻿using UnityEngine;
using System.Collections;

// PUSHES THE BALL IN OPPOSITE DIRECTION WITH A GIVEN FORCE WHEN COLLIDED
public class PushingObstacle : MonoBehaviour 
{
	[SerializeField] private float _force;

	private void OnCollisionEnter2D( Collision2D col )
	{
		var direction = (col.gameObject.transform.position - transform.position);
		var targetForce = direction.normalized * _force;
		col.rigidbody.AddForce (targetForce, ForceMode2D.Force);
	}
}
