﻿using UnityEngine;
using System.Collections;


// APPLIES AI/INPUT FOR SCENE OBJECTS
public class GameSettings : MonoBehaviour 
{
	[SerializeField] private bool _runAI = false;
	[SerializeField] private UnityEngine.Events.UnityEvent _onAI;
	[SerializeField] private UnityEngine.Events.UnityEvent _onUserInput;

	private void Awake()
	{
		var settings = FindObjectOfType<IntermediateSettings> ();
		if ( settings )
		{
			UseAI( settings.UseAI );
		}
		else
		{
			UseAI (_runAI);
		}

	}

	public void UseAI( bool run)
	{
		_runAI = run;

		if ( _runAI )
		{
			_onAI.Invoke ();
		}
		else
		{
			_onUserInput.Invoke ();
		}
	}

}
