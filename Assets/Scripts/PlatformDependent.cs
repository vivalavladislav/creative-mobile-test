﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Events;

public class PlatformDependent : MonoBehaviour
{
	[SerializeField] private bool _runOnAwake;
	[SerializeField] private UnityEvent _onMobile;
	[SerializeField] private UnityEvent _onStandalone;

	private bool _finished;

	private void Start()
	{
		if ( _runOnAwake )
		{
			Run ();
		}
	}

	public void Run()
	{
		if (_finished)
			return;
		
		#if UNITY_STANDALONE 
		_onStandalone.Invoke();
		#endif 

		#if UNITY_IOS || UNITY_ANDROID
		_onMobile.Invoke();
		#endif 

		_finished = true;
	}
}
