﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(HingeJoint2D))]
public class FLIPPER : MonoBehaviour 
{
	[SerializeField] private HingeJoint2D _hinge;

	private bool _activated;
	private float _originalMotorVal;


	private void Awake()
	{
		if ( _hinge == null ){
			_hinge = GetComponent<HingeJoint2D> ();
		}

		if ( _hinge != null )
		{
			_originalMotorVal = _hinge.motor.motorSpeed;
		}
	}

	private void Update()
	{
		if (_hinge == null)
			return;
		
		var sign = _activated ? -1 : 1;
		var motor = _hinge.motor;
		motor.motorSpeed = _originalMotorVal * sign;

		_hinge.motor = motor;
	}

	public void Activate (bool value) 
	{
		_activated = value;
	}

}
