﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

// WHEN COLLIDED WITH A BALL APPLIES FORCE TO A BALL IN DIRECTION OF ITS UP 
public class SpeedUP : MonoBehaviour 
{
	[SerializeField] private float _force;
	[SerializeField] private string _playerTag;
	[SerializeField] private UnityEvent _onStart;
	[SerializeField] private UnityEvent _onFinish;

	private void OnTriggerEnter2D(Collider2D col )
	{
		if (string.IsNullOrEmpty (_playerTag) || col.gameObject.tag == _playerTag)
		{
			_onStart.Invoke ();
			ApplyForce (col);
		}
			
	}

	private void OnTriggerStay2D( Collider2D col )
	{
		if ( string.IsNullOrEmpty( _playerTag ) || col.gameObject.tag == _playerTag )	
		{
			ApplyForce (col);
		}
	}

	private void ApplyForce(Collider2D col )
	{
		var force = transform.up * _force;
		var body = col.GetComponent<Rigidbody2D> ();
		if (body != null)
		{
			body.AddForce (force);
		}
	}
}
