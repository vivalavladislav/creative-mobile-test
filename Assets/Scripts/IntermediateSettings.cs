﻿using UnityEngine;
using System.Collections;

// USED A TEMP STORAGE FOR SETTINGS WHEN RELOADING THE SCENE
public class IntermediateSettings : MonoBehaviour 
{

	public bool UseAI = false;

	public void SetAI( bool use )
	{
		UseAI = use;
	}

	private void Awake()
	{
		DontDestroyOnLoad (gameObject);

		var settings = FindObjectsOfType<IntermediateSettings> ();

		foreach( var sset in settings )
		{
			if ( sset != this )
			{
				Destroy (sset.gameObject);
			}

		}
	}
}
